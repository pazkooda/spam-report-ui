import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ReportEntry} from "../model/report.entry";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = '/api/reports';

  constructor(private httpClient: HttpClient) { }

  getUnresolvedReportEntries() {
    const params = new HttpParams()
      .append("unresolved", "true");

    return this.httpClient.get<ReportEntry[]>(this.apiUrl, {params});
  }

  resolve(id: string) {
    return this.httpClient.put(`${this.apiUrl}/${id}`, { ticketState: 'CLOSED' })
  }

  block(id: string) {
    return this.httpClient.post(`${this.apiUrl}/${id}/block`, null)
  }
}
