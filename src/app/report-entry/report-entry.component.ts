import {Component, Input, OnInit} from '@angular/core';
import {ReportEntry} from "../model/report.entry";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-report-entry',
  templateUrl: './report-entry.component.html',
  styleUrls: ['./report-entry.component.sass']
})
export class ReportEntryComponent implements OnInit {

  @Input()
  entry: ReportEntry;

  constructor(private apiService:ApiService) { }

  ngOnInit() {
  }

  resolve() {
    //cheating here as in 3 hours time there is no way to introduce ngRx store with actions and effects :)
    this.entry.state = "CLOSED";
    this.apiService.resolve(this.entry.id).subscribe()
  }

  block() {
    this.entry.state = "BLOCKING";
    this.apiService.block(this.entry.id).subscribe()
  }
}
