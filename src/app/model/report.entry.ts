export interface Reference {
  referenceId: string;
  referenceType: string;
}

export interface Payload {
  source: string;
  reportType: string;
  message?: any;
  reportId: string;
  referenceResourceId: string;
  referenceResourceType: string;
}

export interface ReportEntry {
  id: string;
  source: string;
  sourceIdentityId: string;
  reference: Reference;
  state: string;
  payload: Payload;
  created: Date;
}

