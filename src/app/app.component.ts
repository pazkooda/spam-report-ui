import {Component, OnInit} from '@angular/core';
import {ApiService} from "./services/api.service";
import {ReportEntry} from "./model/report.entry";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  title = 'spam-report-ui';

  entries: ReportEntry[] = [];

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    console.log("on-init");
    this.apiService.getUnresolvedReportEntries().subscribe(entries => this.entries = entries)
  }

}
